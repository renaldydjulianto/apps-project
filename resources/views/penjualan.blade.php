@extends('layout.template')

@section('title', 'Penjualan')


@section('content')
<a href="/penjualan/print" target="_blank" class="btn btn-primary">Print To Printer</a>
<a href="/penjualan/printpdf" target="_blank" class="btn btn-success">Print To PDF</a>

    <table class="table" id="dataTbl">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">No Faktur</th>
                <th scope="col">Nama Pelanggan</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Kuantitas</th>
                <th scope="col">Total</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; ?>
            @foreach ($penjualan as $data)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $data->id_faktur }}</td>
                    <td>{{ $data->nama_pelanggan }}</td>
                    <td>{{ $data->tanggal_penjualan}}</td>
                    <td>{{ $data->jml_barang}}</td>
                    <td>{{ $data->total}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @endsection