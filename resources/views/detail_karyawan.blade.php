@extends('layout.template')
@section('title', 'Detail Karyawan')
@section('content')

<table class="table">
    <tr>
        <th width="110px">NIP</th>
        <th width="30px">:</th>
        <th>{{ $karyawan->nip }}</th>
    </tr>
    <tr>
        <th width="110px">Nama</th>
        <th width="30px">:</th>
        <th>{{ $karyawan->nama }}</th>
    </tr>
    <tr>
        <th width="110px">Tanggal Lahir</th>
        <th width="30px">:</th>
        <th>{{ $karyawan->tanggal_lahir }}</th>
    </tr>
    <tr>
        <th width="110px">Jenis Kelamin</th>
        <th width="30px">:</th>
        <th>{{ $karyawan->jenis_kelamin }}</th>
    </tr>
    <tr>
        <th width="110px">Department</th>
        <th width="30px">:</th>
        <th>{{ $karyawan->department }}</th>
    </tr>
    <tr>
        <th width="110px">Email</th>
        <th width="30px">:</th>
        <th>{{ $karyawan->email }}</th>
    </tr>
    <tr>
        <th width="110px">Foto</th>
        <th width="30px">:</th>
        <th><img src="{{ url('foto/'.$karyawan->foto) }}" width="200px"></th>
    </tr>
    <tr>
        <th>
            <a href="/karyawan" class="btn btn-sm btn-success">Kembali</a>
        </th>
    </tr>
</table>

@endsection