@extends('layout.template')
@section('title', 'Karyawan')

    

@section('content')
   <a href="/karyawan/add" class="btn btn-sm btn-primary">Add</a>

   @if (session('status'))
   <div class="alert alert-success alert-dismissible">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       <h4><i class="icon fa fa-check"></i> Success!</h4>
       {{ session('status') }}
   </div>
   @endif

    <table class="table" id="dataTbl">
        <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">NIP</th>
            <th scope="col">Tanggal Lahir</th>
            <th scope="col">Jenis Kelamin</th>
            <th scope="col">Department</th>
            <th scope="col">Email</th>
            <th scope="col">Foto</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
            <?php $no=1; ?>
            @foreach ($karyawan as $data)
                
                <tr>
                    <th scope="row">{{ $no++ }}</th>
                    <td>{{ $data->nama }}</td>
                    <td>{{ $data->nip }}</td>
                    <td>{{ $data->tanggal_lahir }}</td>
                    <td>{{ $data->jenis_kelamin }}</td>
                    <td>{{ $data->department }}</td>
                    <td>{{ $data->email }}</td>
                    <td><img src="{{ url('foto/'.$data->foto) }}" width="75px"></td>
                    <td>
                        <a href="/karyawan/detail/{{ $data->id }}" class="btn btn-sm btn-success">Detail</a>
                        <a href="/karyawan/edit/{{ $data->id }}" class="btn btn-sm btn-warning">Edit</a>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete{{ $data->id }}">
                            Delete
                          </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
  
  @foreach ($karyawan as $data)
    <div class="modal modal-danger fade" id="delete{{ $data->id }}">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">{{ $data->nama }}</h4>
            </div>
            <div class="modal-body">
              <p>Apakah Anda Yakin Ingin Menghapus Data Ini ? </p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
              <a href="/karyawan/delete/{{ $data->id }}" class="btn btn-outline">Yes</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
  @endforeach

    @endsection