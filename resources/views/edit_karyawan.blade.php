@extends('layout.template')
@section('title', 'Edit Karyawan')

@section('content')

<form action="/karyawan/update/{{ $karyawan->id }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="content">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Nama</label>
                    <input name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{ $karyawan->nama }}">
                    @error('nama')
                        <div class="invalid-feedback text-danger">{{ $message}} </div>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label>NIP</label>
                    <input name="nip" class="form-control @error('nip') is-invalid @enderror" value="{{$karyawan->nip}}" readonly>
                    @error('nip')
                        <div class="invalid-feedback text-danger">{{ $message}} </div>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" name="tanggal_lahir" class="form-control @error('tanggal_lahir') is-invalid @enderror" value="{{ $karyawan->tanggal_lahir}}">
                    @error('tanggal_lahir')
                        <div class="invalid-feedback text-danger">{{ $message}} </div>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <input name="jenis_kelamin" class="form-control @error('jenis_kelamin') is-invalid @enderror" value="{{ $karyawan->jenis_kelamin }}">
                    @error('jenis_kelamin')
                        <div class="invalid-feedback text-danger">{{ $message}} </div>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label>Department</label>
                    <input name="department" class="form-control @error('department') is-invalid @enderror" value="{{ $karyawan->department }}">
                    @error('department')
                        <div class="invalid-feedback text-danger">{{ $message}} </div>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label>Email</label>
                    <input name="email" class="form-control @error('email') is-invalid @enderror" value="{{ $karyawan->email}}">
                    @error('email')
                        <div class="invalid-feedback text-danger">{{ $message}} </div>
                    @enderror
                </div>
                
                <div class="col-sm 12">
                    <div class="col-sm-4">
                        <img src="{{ url('foto/'.$karyawan->foto) }}" width="100px">
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label>Foto</label>
                            <input type="file" name="foto" class="form-control @error('foto') is-invalid @enderror" value="{{ old('foto')}}">
                            @error('foto')
                                <div class="invalid-feedback text-danger">{{ $message}} </div>
                            @enderror
                        </div>
                    </div>
                    
                </div>


                <div class="form-group">
                    <button class="btn btn-sm btn-primary">Simpan</button>
                </div>
                
            </div>
        </div>
    </div>
                
</form>

@endsection
