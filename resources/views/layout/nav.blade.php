<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li class="{{ request()->is('/') ? 'active' : '' }}">
      <a href="/">
        <i class="fa fa-home"></i> 
        <span>Home</span>
      </a>
    </li>

    @if (auth()->user()->level==1)
      <li class="{{ request()->is('karyawan') ? 'active' : '' }}">
        <a href="/karyawan">
          <i class="fa fa-users"></i> 
          <span>Data Karyawan</span>
        </a>
      </li>

      <li class="{{ request()->is('tanaman') ? 'active' : '' }}">
        <a href="/tanaman">
          <i class="fa fa-leaf"></i> 
          <span>Data Tanaman</span>
        </a>
      </li>

      <li class="{{ request()->is('penjualan') ? 'active' : '' }}">
        <a href="/penjualan">
          <i class="fa fa-book"></i> 
          <span>Data Penjualan</span>
        </a>
      </li>

      <li class="{{ request()->is('user') ? 'active' : '' }}">
        <a href="/user">
          <i class="fa fa-book"></i> 
          <span>User</span>
        </a>
      </li>

      <li class="{{ request()->is('pelanggan') ? 'active' : '' }}">
        <a href="/pelanggan">
          <i class="fa fa-book"></i> 
          <span>Pelanggan</span>
        </a>
      </li>  

      <li class="{{ request()->is('ticket') ? 'active' : '' }}">
        <a href="/ticket">
          <i class="fa fa-ticket"></i> 
          <span>Ticket</span>
        </a>
      </li>  

    @elseif (auth()->user()->level==2)
      <li class="{{ request()->is('penjualan') ? 'active' : '' }}">
        <a href="/penjualan">
          <i class="fa fa-book"></i> 
          <span>Data Penjualan</span>
        </a>
      </li>

      <li class="{{ request()->is('user') ? 'active' : '' }}">
        <a href="/user">
          <i class="fa fa-book"></i> 
          <span>User</span>
        </a>
      </li>
      
    @else
      <li class="{{ request()->is('pelanggan') ? 'active' : '' }}">
        <a href="/pelanggan">
          <i class="fa fa-book"></i> 
          <span>Pelanggan</span>
        </a>
      </li>      
    @endif
  </ul>