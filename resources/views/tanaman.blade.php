@extends('layout.template')

@section('title', 'Tanaman')


@section('content')
<table class="table" id="dataTbl">
    <thead>
    <tr>
        <th scope="col">No</th>
        <th scope="col">Id Tanaman</th>
        <th scope="col">Nama Tanaman</th>
        <th scope="col">Stok Tanaman</th>
        <th scope="col">Jenis</th>
        <th scope="col">Harga Tanaman</th>
    </tr>
    </thead>
    <tbody>
        <?php $no=1; ?>
        @foreach ($tanaman as $data)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $data->id_tanaman }}</td>
                <td>{{ $data->nama_tanaman }}</td>
                <td>{{ $data->stok_tanaman }}</td>
                <td>{{ $data->jenis }}</td>
                <td>{{ $data->harga }}</td>
                
            </tr>
            
        @endforeach
    </tbody>
</table>
@endsection