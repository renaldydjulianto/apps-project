<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KaryawanModel extends Model
{
    public function allData()
    {
        return DB::table('karyawan')->get();
    }

    public function detailData($id)
    {
        return DB::table('karyawan')->where('id', $id)->first();
    }

    public function addData($data)
    {
        DB::table('karyawan')->insert($data);
    }

    public function editData($id, $data)
    {
        DB::table('karyawan')->where('id', $id)->update($data);
    }

    public function deleteData($id)
    {
        DB::table('karyawan')->where('id', $id)->delete();
    }
}
