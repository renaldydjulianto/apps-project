<?php

namespace App\Http\Controllers;

use App\KaryawanModel;
use Illuminate\Http\Request;


class KaryawanController extends Controller
{
    public function __construct()
    {
        $this->KaryawanModel = new KaryawanModel();
        $this->middleware('auth');
    }

    public function index()
    {
        $data = [
            'karyawan' => $this->KaryawanModel->allData(),
        ];
        return view('karyawan', $data);
    }

    public function detail($id)
    {
        if (!$this->KaryawanModel->detailData($id)) {
            abort(404);
        }
        $data = [
            'karyawan' => $this->KaryawanModel->detailData($id),
        ];
        return view('detail_karyawan', $data);
    }

    public function add()
    {
        return view('add_karyawan');
    }

    public function insert()
    {
        request()->validate([
            'nama' => 'required',
            'nip' => 'required|unique:karyawan|min:4|max:5',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'department' => 'required',
            'email' => 'required',
            'foto' => 'required|mimes:jpg,jpeg,bmp,png|max:1024',
        ]);

        //jika validasi tidak ada maka lakukan simpan data
        //upload gambar/foto
        $file = Request()->foto;
        $fileName = Request()->nip . '.' . $file->extension();
        $file->move(public_path('foto'), $fileName);

        $data = [
            'nama' => Request()->nama,
            'nip' => Request()->nip,
            'tanggal_lahir' => Request()->tanggal_lahir,
            'jenis_kelamin' => Request()->jenis_kelamin,
            'department' => Request()->department,
            'email' => Request()->email,
            'foto' => $fileName,
        ];

        $this->KaryawanModel->addData($data);
        return redirect('/karyawan')->with('status', 'Data Karyawan Berhasil Ditambahkan!');
    }

    public function edit($id)
    {
        if (!$this->KaryawanModel->detailData($id)) {
            abort(404);
        }
        $data = [
            'karyawan' => $this->KaryawanModel->detailData($id),
        ];
        return view('edit_karyawan', $data);
    }

    public function update($id)
    {
        request()->validate([
            'nama' => 'required',
            'nip' => 'required|min:4|max:5',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'department' => 'required',
            'email' => 'required',
            'foto' => 'mimes:jpg,jpeg,bmp,png|max:1024',
        ]);

        //jika validasi tidak ada maka lakukan simpan data
        if (Request()->foto <> "") {
            //jika ingin ganti foto
            //upload gambar/foto
            $file = Request()->foto;
            $fileName = Request()->nip . '.' . $file->extension();
            $file->move(public_path('foto'), $fileName);
            $data = [
                'nama' => Request()->nama,
                'nip' => Request()->nip,
                'tanggal_lahir' => Request()->tanggal_lahir,
                'jenis_kelamin' => Request()->jenis_kelamin,
                'department' => Request()->department,
                'email' => Request()->email,
                'foto' => $fileName,
            ];
            $this->KaryawanModel->editData($id, $data);
        } else {
            //jika tidak ingin ganti foto
            $data = [
                'nama' => Request()->nama,
                'nip' => Request()->nip,
                'tanggal_lahir' => Request()->tanggal_lahir,
                'jenis_kelamin' => Request()->jenis_kelamin,
                'department' => Request()->department,
                'email' => Request()->email,
            ];
            $this->KaryawanModel->editData($id, $data);
        }

        return redirect('/karyawan')->with('status', 'Data Karyawan Berhasil Di Update!');
    }

    public function delete($id)
    {
        //hapus atau delete foto
        $karyawan = $this->KaryawanModel->detailData($id);
        if ($karyawan->foto <> "") {
            unlink(public_path('foto') . '/' . $karyawan->foto);
        }

        $this->KaryawanModel->deleteData($id);
        return redirect('/karyawan')->with('status', 'Data Karyawan Berhasil Di Hapus!');
    }
}
