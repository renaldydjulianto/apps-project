<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\VarDumper\Cloner\Data;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'title' => 'Dashboard', 
            'karyawan' => DB::table('karyawan')->count(),
            'tanaman' => DB::table('tanaman')->count(),
            'penjualan' => DB::table('penjualan')->count(),
            'user' => DB::table('users')->count()
        ];
        return view('home', $data);
    }
}
