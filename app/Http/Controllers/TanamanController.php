<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TanamanModel;

class TanamanController extends Controller
{
    public function __construct()
    {
        $this->TanamanModel = new TanamanModel();
        $this->middleware('auth');
    }

    public function index()
    {
        $data = [
            'tanaman' => $this->TanamanModel->allData(),
        ];
        return view('tanaman', $data);
    }
}
