<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TanamanModel extends Model
{
    public function allData()
    {
        return DB::table('tanaman')
            ->leftJoin('jenis', 'jenis.id_jenis', '=', 'tanaman.id_jenis')
            ->leftJoin('harga', 'harga.id_harga', '=', 'tanaman.id_harga')
            ->get();
    }
}
