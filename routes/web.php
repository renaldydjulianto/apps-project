<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\KaryawanController;
use App\Http\Controllers\TanamanController;
use App\Http\Controllers\PenjualanController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PelangganController;
use App\Http\Controllers\TicketController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

//karyawan




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');





//hak akses untuk admin
Route::group(['middleware' => 'admin'], function () {
    //karyawan
    Route::get('/karyawan', [KaryawanController::class, 'index'])->name('karyawan');
    Route::get('/karyawan/detail/{id}', [KaryawanController::class, 'detail']);
    Route::get('/karyawan/add', [KaryawanController::class, 'add']);
    Route::post('/karyawan/insert', [KaryawanController::class, 'insert']);
    Route::get('/karyawan/edit/{id}', [KaryawanController::class, 'edit']);
    Route::post('/karyawan/update/{id}', [KaryawanController::class, 'update']);
    Route::get('/karyawan/delete/{id}', [KaryawanController::class, 'delete']);

    //Tanaman
    Route::get('/tanaman', [TanamanController::class, 'index']);

    //User
    Route::get('/user', [UserController::class, 'index']);

    //Ticket
    Route::get('/ticket', [TicketController::class, 'index']);

    //Penjualan
    Route::get('/penjualan', [PenjualanController::class, 'index']);
    Route::get('/penjualan/print', [PenjualanController::class, 'print']);
    Route::get('/penjualan/printpdf', [PenjualanController::class, 'printpdf']);

    //Pelanggan
    Route::get('/pelanggan', [PelangganController::class, 'index']);
});

Route::group(['middleware' => 'user'], function () {
    //User
    //Route::get('/user', [UserController::class, 'index']);
});

Route::group(['middleware' => 'pelanggan'], function () {
    //Pelanggan
    //Route::get('/pelanggan', [PelangganController::class, 'index']);
});
